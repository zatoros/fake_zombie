--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--pakiet obslugi pomocy

WITH Ada.Text_IO; USE Ada.Text_IO;
WITH Nt_Console; USE Nt_Console;
with ada.Calendar; use ada.Calendar;
WITH Ekran_Pac; USE Ekran_Pac;
WITH Ada.Numerics.Discrete_Random;

package body Pomoc is
PROCEDURE Pomoc_Gry(Koniec : IN OUT Boolean; Kolory : IN OUT Character) IS
   PACKAGE G IS NEW Ada.Numerics.Discrete_Random(Integer);
   USE G;
   --zadanie opisuje animacje "buziek" w menu pomocy
   TASK TYPE Ziomek IS
      ENTRY Inicjalizuj(Xs : X_Pos; Ys : Y_Pos; Gen : Integer; Xz1,Xz2 : X_Pos;Yz1,Yz2 : Y_Pos);
      ENTRY Koniec;
   END Ziomek;

   Ekran_Pomocy : Ekran;

   TASK BODY Ziomek IS
      Ox : Generator;
      X,Xn : X_Pos;
      Y,Yn: Y_Pos;
      Nast : Time;
      Ruch : constant Duration := 0.05;
      Xp,Xk : X_Pos;
      Yp,Yk : Y_Pos;
   BEGIN
      ACCEPT Inicjalizuj(Xs : X_Pos; Ys : Y_Pos; Gen : Integer; Xz1,Xz2 : X_Pos;Yz1,Yz2 : Y_Pos) DO
         X := Xs;
         Y := Ys;
      Reset(Ox,Gen);
      Xp:=Xz1; Xk:=Xz2;
         Yp:= Yz1; Yk:=Yz2;
         Ekran_Pomocy.Dodaj_Pole(Xs,Ys,Character'Val(1));
      END Inicjalizuj;
      Nast := Clock;
      LOOP
         DELAY UNTIL Nast;
         Xn := (X+ ((Random(Ox) mod 3))-1);
         Yn := (Y+ ((Random(Ox) mod 3))-1);
         if Xn not IN Xp..Xk then
            Xn := X-(Xn-X);
         END IF;
         IF Yn  NOT IN Yp..Yk THEN
            Yn := Y-(Yn-Y);
         END IF;
           Ekran_Pomocy.Przestaw_Pole(X,Xn,Y,Yn);
           X:= Xn; Y:= Yn;
      SELECT
         ACCEPT Koniec DO
               Xn := 0;
         END Koniec;
         ELSE
            NULL;
         END SELECT;
         EXIT WHEN Xn = 0;
      Nast := Nast + Ruch;
   END LOOP;
END Ziomek;

   X : Character;
   Ziomki : ARRAY (1..4) OF Ziomek;
BEGIN
   Ekran_Pomocy.Inicjuj_Pomoc;
   Koniec := NOT Ekran_Pomocy.Obsluz_Wejscie_Standard(Kolory);
   Ziomki(1).Inicjalizuj(2,2,1,1,10,2,22);
   Ziomki(2).Inicjalizuj(70,2,2,69,78,2,22);
   Ziomki(3).Inicjalizuj(11,3,3,11,68,2,3);
   Ziomki(4).Inicjalizuj(65,21,4,11,68,21,22);

   LOOP
      Get_Immediate(X);
      IF NOT Ekran_Pomocy.Obsluz_Wejscie_Standard(X) THEN
         FOR I IN 1..4 LOOP
            ABORT Ziomki(I);
         END LOOP;
         Koniec := True;
         EXIT;
      END IF;
      IF X IN '0'..'3' THEN
         Kolory := X;
      END IF;

      IF Character'Pos(X) = 13 THEN
         FOR I IN 1..4 LOOP
            Ziomki(I).Koniec;
         END LOOP;
         EXIT;
      END IF;
   END LOOP;
EXCEPTION
   WHEN OTHERS =>
      Put("Nieznany wyjatek w PROCEDURZE POMOC_GRY");
END Pomoc_Gry;
END Pomoc;

