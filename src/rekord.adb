--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--pakiet do obslugi rekordow z gry

WITH Ekran_Pac;
USE Ekran_Pac;
WITH Nt_Console;
USE Nt_Console;
WITH Ada.Calendar;
USE Ada.Calendar;
WITH Ada.Text_IO;
USE Ada.Text_IO;

PACKAGE BODY Rekord IS
   PROCEDURE Rekordy (Koniec : IN OUT Boolean; Kolory : IN OUT Character) IS
      TYPE Kierunek IS (GL, GP, DL, DP);
      --zadanie opisuje animowane pociski w menu rekordow
      TASK TYPE Pocisk IS
         ENTRY Inicjalizuj (
               Xs  : X_Pos;
               Ys  : Y_Pos;
               Xz1,
               Xz2 : X_Pos;
               Yz1,
               Yz2 : Y_Pos;
               K   : Kierunek);
         ENTRY Koniec;
      END Pocisk;

      Ekran_Rekordy : Ekran;
      Rekordy       : Rekordzisci;
      X             : Character;

      TASK BODY Pocisk IS
         X,
         Xn   :          X_Pos;
         Y,
         Yn   :          Y_Pos;
         Nast :          Time;
         Ruch : CONSTANT Duration := 0.1;
         Xp,
         Xk   :          X_Pos;
         Yp,
         Yk   :          Y_Pos;
         Cel  :          Kierunek;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs  : X_Pos;
               Ys  : Y_Pos;
               Xz1,
               Xz2 : X_Pos;
               Yz1,
               Yz2 : Y_Pos;
               K   : Kierunek) DO
            X := Xs;
            Y := Ys;
            Xp:=Xz1;
            Xk:=Xz2;
            Yp:= Yz1;
            Yk:=Yz2;
            Cel := K;
            IF Cel = GL OR Cel = DP THEN
               Ekran_Rekordy.Dodaj_Pole(Xs,Ys,Character'Val(47));
            ELSE
               Ekran_Rekordy.Dodaj_Pole(Xs,Ys,Character'Val(92));
            END IF;
         END Inicjalizuj;
         Nast := Clock;
         LOOP
            DELAY UNTIL Nast;
            CASE Cel IS
               WHEN GL =>
                  Xn := X-1;
                  Yn := Y+1;
               WHEN GP =>
                  Xn := X+1;
                  Yn := Y+1;
               WHEN DL =>
                  Xn := X-1;
                  Yn := Y-1;
               WHEN DP =>
                  Xn := X+1;
                  Yn := Y-1;
            END CASE;
            IF Xn NOT IN Xp..Xk THEN
               Xn := X-(Xn-X);
               CASE Cel IS
                  WHEN GL =>
                     Cel := GP;
                  WHEN GP =>
                     Cel := GL;
                  WHEN DL =>
                     Cel := DP;
                  WHEN DP =>
                     Cel := DL;
               END CASE;
               IF (Xn-X)*(Yn-Y) > 0 THEN
                  Ekran_Rekordy.Dodaj_Pole(X,Y,Character'Val(92));
               ELSE
                  Ekran_Rekordy.Dodaj_Pole(X,Y,Character'Val(47));
               END IF;
            END IF;
            IF Yn  NOT IN Yp..Yk THEN
               Yn := Y-(Yn-Y);
               CASE Cel IS
                  WHEN GL =>
                     Cel := DL;
                  WHEN GP =>
                     Cel := DP;
                  WHEN DL =>
                     Cel := GL;
                  WHEN DP =>
                     Cel := GP;
               END CASE;
               IF (Xn-X)*(Yn-Y) > 0 THEN
                  Ekran_Rekordy.Dodaj_Pole(X,Y,Character'Val(92));
               ELSE
                  Ekran_Rekordy.Dodaj_Pole(X,Y,Character'Val(47));
               END IF;
            END IF;
            Ekran_Rekordy.Przestaw_Pole(X,Xn,Y,Yn);
            X:= Xn;
            Y:= Yn;
            SELECT
               ACCEPT Koniec DO
                  Xn := 0;
               END Koniec;
            ELSE
               NULL;
            END SELECT;
            EXIT WHEN Xn = 0;
            Nast := Nast + Ruch;
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznay wyjatek w ZADANIU POCISK");
      END Pocisk;

      Pociski : ARRAY (1 .. 4) OF Pocisk;
   BEGIN
      Odczytaj_Rekordy(Rekordy);
      Ekran_Rekordy.Inicjuj_Rekordy(Rekordy);
      Koniec := NOT Ekran_Rekordy.Obsluz_Wejscie_Standard(Kolory);
      Pociski(1).Inicjalizuj(3,5,1,28,2,22,DP);
      Pociski(2).Inicjalizuj(69,4,51,78,2,22,GL);
      Pociski(3).Inicjalizuj(30,3,30,49,2,4,GP);
      Pociski(4).Inicjalizuj(48,21,30,49,20,22,GL);
      LOOP
         Get_Immediate(X);
         IF NOT Ekran_Rekordy.Obsluz_Wejscie_Standard(X) THEN
            FOR I IN 1..4 LOOP
               ABORT Pociski(I);
            END LOOP;
            Koniec := True;
            EXIT;
         END IF;
         IF X IN '0'..'3' THEN
            Kolory := X;
         END IF;

         IF Character'Pos(X) = 13 THEN
            FOR I IN 1..4 LOOP
               Pociski(I).Koniec;
            END LOOP;
            EXIT;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS =>
         Put("Nieznany wyjatek w PROCEDURZE REKORDY");
   END Rekordy;
END Rekord;





