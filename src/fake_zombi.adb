--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--procedura gl�wna gry

WITH Menu; USE Menu;
WITH Gra; USE Gra;
WITH Przewodnik; USE Przewodnik;
WITH Rekord; USE Rekord;
WITH Pomoc; USE Pomoc;
WITH Ada.Text_IO; USE Ada.Text_IO;


PROCEDURE Fake_Zombi IS
   I          : Integer;
   Czy_Koniec : Boolean   := False;
   Kolory     : Character := '0';
   Dane_Gra_Wrogowie : Integer;
   Dane_Gra_Poziom : Poziom_Trudnosci;
BEGIN
   LOOP
      Menu_Gry(I,Kolory);
      CASE I IS
         WHEN 1 =>
            Wczytaj_Dane_Gra(Kolory,Dane_Gra_Poziom,Dane_Gra_Wrogowie);
            Nowa_Gra(Czy_Koniec,Kolory,Dane_Gra_Poziom,Dane_Gra_Wrogowie);
            EXIT WHEN Czy_Koniec;
            Rekordy(Czy_Koniec,Kolory);
         WHEN 2 =>
            Tutorial(Czy_Koniec,Kolory);
         WHEN 3 =>
            Rekordy(Czy_Koniec,Kolory);
         WHEN 4 =>
            Pomoc_Gry(Czy_Koniec,Kolory);
         WHEN OTHERS =>
            Czy_Koniec := True;
      END CASE;
      EXIT WHEN Czy_Koniec;
   END LOOP;
EXCEPTION
   WHEN OTHERS =>
      Put("Nieznany wyjatek w PROCEDURZE GLOWNEJ");
END Fake_Zombi;



