--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--pakiet z obsluga menu

WITH Ada.Text_IO; USE Ada.Text_IO;
WITH Nt_Console; USE Nt_Console;
WITH Ekran_Pac; USE Ekran_Pac;
WITH Ada.Numerics.Discrete_Random;
WITH Ada.Calendar; USE Ada.Calendar;

PACKAGE BODY Menu IS
   PROCEDURE Menu_Gry (Opcja : IN OUT Integer; Kolory : IN OUT Character)  IS

      SUBTYPE Wybory IS Integer RANGE 7..13;

      PACKAGE G IS NEW Ada.Numerics.Discrete_Random(Integer);
      USE G;
      --zmienne uzyte w programie
      Ekran_Menu  : Ekran;
      Wybor       : Wybory    := 8;
      Temp        : Integer;
      X           : Character;
      Koniec_Menu : Boolean   := False;
      PRAGMA Atomic(Koniec_Menu);
      --zadanie opisuje animacje latajacej "buzki" na ekranie menu
      TASK TYPE Gwiazdka IS
         ENTRY Inicjalizuj (
               Xs  : X_Pos;
               Ys  : Y_Pos;
               Gen : Integer;
               Xz1,
               Xz2 : X_Pos;
               Yz1,
               Yz2 : Y_Pos);
      END Gwiazdka;

      TASK BODY Gwiazdka IS
         Ox   :          Generator;
         X,
         Xn   :          X_Pos;
         Y,
         Yn   :          Y_Pos;
         Nast :          Time;
         Ruch : CONSTANT Duration  := 0.5;
         Xp,
         Xk   :          X_Pos;
         Yp,
         Yk   :          Y_Pos;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs  : X_Pos;
               Ys  : Y_Pos;
               Gen : Integer;
               Xz1,
               Xz2 : X_Pos;
               Yz1,
               Yz2 : Y_Pos) DO
            X := Xs;
            Y := Ys;
            Reset(Ox,Gen);
            Xp:=Xz1;
            Xk:=Xz2;
            Yp:= Yz1;
            Yk:=Yz2;
            Ekran_Menu.Dodaj_Pole(Xs,Ys,Character'Val(1));
         END Inicjalizuj;
         Nast := Clock;
         LOOP
            DELAY UNTIL Nast;
            Xn := (Random(Ox) mod (Xk-Xp+1))+Xp;
            Yn := (Random(Ox) mod (Yk-Yp+1))+Yp;
            Ekran_Menu.Przestaw_Pole(X,Xn,Y,Yn);
            X:= Xn;
            Y:= Yn;
            EXIT WHEN Koniec_Menu;
            Nast := Nast + Ruch;
         END LOOP;
      END Gwiazdka;

      Gwiazdki : ARRAY (1 .. 25) OF Gwiazdka;
   BEGIN
      Ekran_Menu.Inicjuj_Menu;
      Koniec_Menu := NOT Ekran_Menu.Obsluz_Wejscie_Standard(Kolory);
      --tu nastepuje inicjalizacja poczatkowych polozen "buziek"
      FOR I IN 1..10 LOOP
         Gwiazdki(I).Inicjalizuj(1,(I+1),I,1,26,3,22);
      END LOOP;
      FOR I IN 11..15 LOOP
         Gwiazdki(I).Inicjalizuj(37,(I+3),I,27,50,14,22);
      END LOOP;
      FOR I IN 16..25 LOOP
         Gwiazdki(I).Inicjalizuj(78,(I-14),I,51,78,2,22);
      END LOOP;

      LOOP
         Get_Immediate(X);
         IF NOT Ekran_Menu.Obsluz_Wejscie_Standard(X) THEN
            FOR I IN 1..25 LOOP
               ABORT Gwiazdki(I);
            END LOOP;
            Opcja := 5;
            EXIT;
         END IF;
         IF X IN '0'..'3' THEN
            Kolory := X;
         END IF;
         IF Character'Pos(X) = 224 THEN
            Get_Immediate(X);
            IF X = 'H' THEN     --strzalka w gore
               Temp := Wybor;
               Wybor := Wybor - 1;
               IF Wybor = 7 THEN
                  Wybor := 12;
               END IF;
               Ekran_Menu.Przestaw_Pole(32,32,Temp,Wybor);
            ELSIF X = 'P' THEN   --strzalka w dol
               Temp := Wybor;
               Wybor := Wybor + 1;
               IF Wybor = 13 THEN
                  Wybor := 8;
               END IF;
               Ekran_Menu.Przestaw_Pole(32,32,Temp,Wybor);
            END IF;
         END IF;

         IF Character'Pos(X) = 13 THEN  --enter
            CASE Wybor IS
               WHEN 8 =>
                  Opcja := 1;
               WHEN 9 =>
                  Opcja := 2;
               WHEN 10 =>
                  Opcja := 3;
               WHEN 11 =>
                  Opcja := 4;
               WHEN OTHERS =>
                  Opcja := 5;
            END CASE;
            Koniec_Menu := True;
            EXIT;
         END IF;

      END LOOP;
   EXCEPTION
      WHEN OTHERS =>
         Put("Nieznany wyjatek w PROCEDURZE MENU");
   END Menu_Gry;
END Menu;






