--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--Pakiet z obsluga tutoriala gry

WITH Nt_Console; USE Nt_Console;
WITH Nt_Mouse; USE Nt_Mouse;
WITH Ekran_Pac; USE Ekran_Pac;
WITH Ada.Calendar; USE Ada.Calendar;
WITH Ada.Text_IO; USE Ada.Text_IO;

PACKAGE BODY Przewodnik IS
   PROCEDURE Tutorial (Koniec : IN OUT Boolean; Kolory : IN OUT Character) IS
      --typ opisujacy kierunek strzalow
      TYPE Kierunek IS (G, GL, GP, L, P, D, DL, DP);
      --typ opisujacy kolejne wyrany podpowiedzi
      TYPE Hints IS ARRAY (0 .. 5) OF String (1 .. 55);

      Ekran_Tutorial : Ekran;
      --stale opisujace min. rozmiar planszy
      Xp              : CONSTANT X_Pos   := 1;
      Xk              : CONSTANT X_Pos   := 78;
      Yp              : CONSTANT Y_Pos   := 2;
      Yk              : CONSTANT Y_Pos   := 20;
      Pociski_Plansza :          Integer := 0;
      --ilosc maxymalna nieruchomych wrogow i struktuta danych do przechowywanie ich wspolzendnych
      Ilosc_Kukl : CONSTANT Integer := 3;
      TYPE Zderzx IS ARRAY (1 .. Ilosc_Kukl) OF X_Pos;
      TYPE Zderzy IS ARRAY (1 .. Ilosc_Kukl) OF Y_Pos;

      --Obiekt chroniony obslugujacy zderzenia pociskow z nieruchomymi wrogami
      PROTECTED Do_Zderzenia IS
         PROCEDURE Ustaw_Wroga (
               X : X_Pos;
               Y : Y_Pos);
         PROCEDURE Zrob_Zderzenia (
               X   : X_Pos;
               Y   : Y_Pos;
               Xpo : X_Pos;
               Ypo : Y_Pos);
      PRIVATE
         Xn : Zderzx;
         Yn: Zderzy;
         Liczba : Integer := 1;
      END Do_Zderzenia;

      --typ zadaniowy opisuje pocisk
      TASK TYPE Pocisk IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos;
               K  : Kierunek);
         ENTRY Koniec;
      END Pocisk;
      --typ zadaniowy opisuje nieruchomego wroga
      TASK TYPE Zombi IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos);
         ENTRY Zderzenie (
               X : X_Pos;
               Y : Y_Pos);
         ENTRY Koniec;
      END Zombi;
      --typ zadaniowy opisuje gracza
      TASK TYPE Gracz IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos);
         ENTRY Ruch (
               K : Kierunek);
         ENTRY Koniec;
         ENTRY Strzal (
               X : X_Pos;
               Y : Y_Pos);
      END Gracz;
      --typ zadaniowy pokazujacy podpowiedzi w czasie odbywania tutoriala
      TASK TYPE Podpowiedzi IS
         ENTRY Inicjalizuj (
               H      : Hints;
               Liczba : Integer);
         ENTRY Koniec;
      END Podpowiedzi;

      TASK BODY Podpowiedzi IS
         Przerwa    : CONSTANT Duration := 5.0;
         Index      :          Integer  := 0;
         Czy_Koniec :          Boolean  := False;
         Napisy     :          Hints;
         Licznik    :          Integer;
      BEGIN
         ACCEPT Inicjalizuj (
               H      : Hints;
               Liczba : Integer) DO
            Napisy := H;
            Licznik := Liczba;
         END Inicjalizuj;
         FOR I IN Napisy(Index)'RANGE LOOP
            Ekran_Tutorial.Dodaj_Pole((I+1),22,Napisy(Index)(I));
         END LOOP;
         LOOP
            SELECT
               ACCEPT Koniec DO
                  Czy_Koniec:= True;
               END Koniec;
            OR
               DELAY Przerwa;
               FOR I IN Napisy(Index)'RANGE LOOP
                  Ekran_Tutorial.Dodaj_Pole((I+1),22,Napisy(Index)(I));
               END LOOP;
               Index := (Index mod Licznik)+1;
            END SELECT;
            EXIT WHEN Czy_Koniec;
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznany wyjatek w ZADANIU PODPOWIEDZI");
      END Podpowiedzi;

      TASK BODY Gracz IS
         Xg,
         Xt,
         Xpo             :          X_Pos;
         Yg,
         Yt,
         Ypo             :          Y_Pos;
         Kpo             :          Kierunek;
         Czy_Koniec      :          Boolean  := False;
         Liczba_Strzalow : CONSTANT Integer  := 5;
         TYPE Index IS mod Liczba_Strzalow;
         Licznik :          Index                   := 0;
         Pociski :          ARRAY (Index) OF Pocisk;
         Nast    :          Time;
         Przerwa : CONSTANT Duration                := 0.1;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos) DO
            Xg := Xs;
            Yg := Ys;
            Ekran_Tutorial.Dodaj_Pole(Xg,Yg,Character'Val(2));
         END Inicjalizuj;
         Nast := Clock;
         LOOP
            DELAY UNTIL Nast;
            SELECT
               ACCEPT Ruch (
                     K : Kierunek) DO
                  CASE K IS
                     WHEN G=>
                        Xt := Xg;
                        Yt := Yg-1;
                     WHEN L=>
                        Xt := Xg-1;
                        Yt := Yg;
                     WHEN P=>
                        Xt := Xg+1;
                        Yt := Yg;
                     WHEN D=>
                        Xt := Xg;
                        Yt := Yg+1;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
                  IF Ekran_Tutorial.Wolne_Pole(Xt,Yt) THEN
                     Ekran_Tutorial.Przestaw_Pole(Xg,Xt,Yg,Yt);
                     Xg := Xt;
                     Yg := Yt;
                  END IF;
               END Ruch;
            OR
               ACCEPT Strzal (
                     X : X_Pos;
                     Y : Y_Pos) DO
                  IF Pociski_Plansza < Liczba_Strzalow THEN
                     IF X > Xg AND Y = Yg THEN
                        Xpo := Xg+1;
                        Ypo:=Yg;
                        Kpo :=P;
                     ELSIF X > Xg AND Y < Yg THEN
                        Xpo:=Xg+1;
                        Ypo:=Yg-1;
                        Kpo:=GP;
                     ELSIF X > Xg AND Y > Yg THEN
                        Xpo:=Xg+1;
                        Ypo:=Yg+1;
                        Kpo:=DP;
                     ELSIF X = Xg AND Y < Yg THEN
                        Xpo:=Xg;
                        Ypo:=Yg-1;
                        Kpo:=G;
                     ELSIF X = Xg AND Y > Yg THEN
                        Xpo:=Xg;
                        Ypo:=Yg+1;
                        Kpo:=D;
                     ELSIF X < Xg AND Y = Yg THEN
                        Xpo:=Xg-1;
                        Ypo:=Yg;
                        Kpo:=L;
                     ELSIF X < Xg AND Y < Yg THEN
                        Xpo:=Xg-1;
                        Ypo:=Yg-1;
                        Kpo:=GL;
                     ELSIF X < Xg AND Y > Yg THEN
                        Xpo:=Xg-1;
                        Ypo:=Yg+1;
                        Kpo:=DL;
                     END IF;
                     IF Ekran_Tutorial.Wolne_Pole(Xpo,Ypo) THEN
                        Pociski(Licznik).Inicjalizuj(Xpo,Ypo,Kpo);
                        Licznik := Licznik+1;
                     END IF;
                  END IF;
               END Strzal;
            OR
               ACCEPT Koniec DO
                  Czy_Koniec:=True;
               END Koniec;
            END SELECT;
            IF Czy_Koniec THEN
               FOR I IN Index LOOP
                  Pociski(I).Koniec;
               END LOOP;
               EXIT;
            END IF;
            Nast := Nast + Przerwa;
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznany wyjatek w ZADANIU GRACZ");
      END Gracz;

      TASK BODY Pocisk IS
         Xpo,
         Xt         :          X_Pos;
         Ypo,
         Yt         :          Y_Pos;
         Cel        :          Kierunek;
         Lot        :          Integer;
         Czy_Koniec :          Boolean  := False;
         Zasieg     : CONSTANT Integer  := 15;
         Szybkosc   : CONSTANT Duration := 0.05;
         Nast       :          Time;
      BEGIN
         LOOP
            SELECT
               ACCEPT Inicjalizuj (
                     Xs : X_Pos;
                     Ys : Y_Pos;
                     K  : Kierunek) DO
                  Xpo := Xs;
                  Ypo := Ys;
                  Cel := K;
                  Lot := 0;
                  Pociski_Plansza := Pociski_Plansza+1;
                  CASE K IS
                     WHEN G | D =>
                        Ekran_Tutorial.Dodaj_Pole(Xs,Ys,Character'Val(124));
                     WHEN L | P =>
                        Ekran_Tutorial.Dodaj_Pole(Xs,Ys,Character'Val(45));
                     WHEN GL | DP =>
                        Ekran_Tutorial.Dodaj_Pole(Xs,Ys,Character'Val(92));
                     WHEN GP | DL =>
                        Ekran_Tutorial.Dodaj_Pole(Xs,Ys,Character'Val(47));
                  END CASE;
               END Inicjalizuj;
            OR
               ACCEPT Koniec DO
                  Czy_Koniec := True;
               END Koniec;
            END SELECT;
            Nast := Clock + Szybkosc;
            EXIT WHEN Czy_Koniec;
            LOOP
               IF Lot = Zasieg THEN
                  Ekran_Tutorial.Dodaj_Pole(Xpo,Ypo,' ');
                  EXIT;
               END IF;
               DELAY UNTIL Nast;
               CASE Cel IS
                  WHEN G =>
                     Xt := Xpo;
                     Yt := Ypo-1;
                  WHEN GL=>
                     Xt := Xpo-1;
                     Yt := Ypo-1;
                  WHEN GP =>
                     Xt := Xpo+1;
                     Yt := Ypo-1;
                  WHEN L =>
                     Xt := Xpo-1;
                     Yt := Ypo;
                  WHEN P =>
                     Xt := Xpo+1;
                     Yt := Ypo;
                  WHEN D =>
                     Xt := Xpo;
                     Yt := Ypo+1;
                  WHEN DL =>
                     Xt := Xpo-1;
                     Yt := Ypo+1;
                  WHEN DP =>
                     Xt := Xpo+1;
                     Yt := Ypo+1;
               END CASE;
               IF Xt NOT IN Xp..Xk THEN
                  Xt := Xpo -(Xt-Xpo);
                  CASE Cel IS
                     WHEN GL=>
                        Cel := GP;
                     WHEN GP =>
                        Cel := GL;
                     WHEN L =>
                        Cel := P;
                     WHEN P =>
                        Cel := L;
                     WHEN DL =>
                        Cel := Dp;
                     WHEN DP =>
                        Cel := DL;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
               END IF;
               IF Yt NOT IN Yp..Yk THEN
                  Yt := Ypo - (Yt-Ypo);
                  CASE Cel IS
                     WHEN G =>
                        Cel := D;
                     WHEN GL=>
                        Cel := DL;
                     WHEN GP =>
                        Cel := DP;
                     WHEN D =>
                        Cel := G;
                     WHEN DL =>
                        Cel := GL;
                     WHEN DP =>
                        Cel := GP;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
               END IF;
               CASE Cel IS
                  WHEN G | D =>
                     Ekran_Tutorial.Dodaj_Pole(Xpo,Ypo,Character'Val(124));
                  WHEN L | P =>
                     Ekran_Tutorial.Dodaj_Pole(Xpo,Ypo,Character'Val(45));
                  WHEN GL | DP =>
                     Ekran_Tutorial.Dodaj_Pole(Xpo,Ypo,Character'Val(92));
                  WHEN GP | DL =>
                     Ekran_Tutorial.Dodaj_Pole(Xpo,Ypo,Character'Val(47));
               END CASE;
               IF Ekran_Tutorial.Wolne_Pole(Xt,Yt) THEN
                  Ekran_Tutorial.Przestaw_Pole(Xpo,Xt,Ypo,Yt);
                  Xpo := Xt;
                  Ypo := Yt;
               ELSIF Ekran_Tutorial.Wrogie_Pole(Xt,Yt) THEN
                  Do_Zderzenia.Zrob_Zderzenia(Xt,Yt,Xpo,Ypo);
                  Lot := Zasieg-1;
               END IF;
               Lot := Lot+1;
               Nast := Nast + Szybkosc;
            END LOOP;
            Pociski_Plansza := Pociski_Plansza-1;
         END LOOP;
      EXCEPTION
         WHEN OTHERS=>
            Put("Nieznany wyjatek w ZADANIU POCISK");
      END Pocisk;

      TASK BODY Zombi IS
         Xz         :          X_Pos;
         Yz         :          Y_Pos;
         Przerwa    : CONSTANT Duration := 5.0;
         Czy_Koniec :          Boolean  := False;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos) DO
            Xz := Xs;
            Yz := Ys;
            Ekran_Tutorial.Dodaj_Pole(Xs,Ys,Character'Val(1));
         END Inicjalizuj;
         LOOP
            SELECT
               ACCEPT Zderzenie (
                     X : X_Pos;
                     Y : Y_Pos) DO
                  Ekran_Tutorial.Dodaj_Pole(Xz,Yz,' ');
                  Ekran_Tutorial.Dodaj_Pole(X,Y,' ');
               END Zderzenie;
            OR
               ACCEPT Koniec DO
                  Czy_Koniec := True;
               END Koniec;
            END SELECT;
            EXIT WHEN Czy_Koniec;
            DELAY Przerwa;
            WHILE NOT Ekran_Tutorial.Wolne_Pole(Xz,Yz) LOOP
               DELAY Przerwa;
            END LOOP;
            Ekran_Tutorial.Dodaj_Pole(Xz,Yz,Character'Val(1));
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznay wyjatek w ZADANIU ZOMBI");
      END Zombi;


      X,
      Y                 :          Integer;
      Buttons,
      Controlk          :          Unsigned;
      Asci              :          Character;
      Virtualsc         :          Integer;
      Hinty             : CONSTANT Hints                            :=
         ("Witaj w TUTORIALU, nauczysz sie tu podstaw rozgrywki   ",
         "Uzywaj strzalek by poruszac sie w odpowiednim kierunku ",
         "Kliknij w oknie konsoli by oddac strzal                ",
         "Sproboj trafic stojace zombiaki                        ",
         "By wrocic do menu nacisnij enter                       ",
            "Powodzenia w prawdziwej grze                           ");
      Pokaz_Podpowiedzi :          Podpowiedzi;
      Zawodnik          :          Gracz;
      Kukly             :          ARRAY (1 .. Ilosc_Kukl) OF Zombi;

      PROTECTED BODY Do_Zderzenia IS
         PROCEDURE Ustaw_Wroga (
               X : X_Pos;
               Y : Y_Pos) IS
         BEGIN
            Xn(Liczba) := X;
            Yn(Liczba):=Y;
            Liczba:=Liczba+1;
         END Ustaw_Wroga;

         PROCEDURE Zrob_Zderzenia (
               X   : X_Pos;
               Y   : Y_Pos;
               Xpo : X_Pos;
               Ypo : Y_Pos) IS
         BEGIN
            FOR I IN 1..Ilosc_Kukl LOOP
               IF X = Xn(I) AND Y = Yn(I) THEN
                  Kukly(I).Zderzenie(Xpo,Ypo);
               END IF;
            END LOOP;
         END Zrob_Zderzenia;

      END Do_Zderzenia;
   BEGIN
      Ekran_Tutorial.Inicjuj_Tutorial;
      Koniec := NOT Ekran_Tutorial.Obsluz_Wejscie_Standard(Kolory);
      Pokaz_Podpowiedzi.Inicjalizuj(Hinty,5);
      Zawodnik.Inicjalizuj(40,10);
      Kukly(1).Inicjalizuj(5,5);
      Do_Zderzenia.Ustaw_Wroga(5,5);
      Kukly(2).Inicjalizuj(5,10);
      Do_Zderzenia.Ustaw_Wroga(5,10);
      Kukly(3).Inicjalizuj(5,15);
      Do_Zderzenia.Ustaw_Wroga(5,15);
      LOOP
         Get_Anything(X,Y,Buttons,Asci,Virtualsc,Controlk);

         IF NOT Ekran_Tutorial.Obsluz_Wejscie_Standard(Asci) THEN
            ABORT
            Pokaz_Podpowiedzi;
            ABORT Zawodnik;
            FOR I IN 1..3 LOOP
               ABORT Kukly(I);
               Koniec := True;
            END LOOP;
            EXIT;
         END IF;
         IF Asci IN '0'..'3' THEN
            Kolory := Asci;
         END IF;

         IF Buttons = From_Left_1st_Button_Pressed THEN
            Zawodnik.Strzal(X,Y);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Left" THEN
            Zawodnik.Ruch(L);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Right" THEN
            Zawodnik.Ruch(P);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Up" THEN
            Zawodnik.Ruch(G);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Down" THEN
            Zawodnik.Ruch(D);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Enter" THEN
            Pokaz_Podpowiedzi.Koniec;
            Zawodnik.Koniec;
            FOR I IN 1..3 LOOP
               Kukly(I).Koniec;
            END LOOP;
            EXIT;
         END IF;

      END LOOP;
   EXCEPTION
      WHEN OTHERS =>
         Put("Nieznany wyjatek w PROCEDURZE TUTORIAL");
   END Tutorial;
END Przewodnik;



