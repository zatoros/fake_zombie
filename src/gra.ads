--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
PACKAGE Gra IS
   TYPE Poziom_Trudnosci IS (TRUDNY,SREDNI,PROSTY);
   PROCEDURE Wczytaj_Dane_Gra(Kolory : IN Character; Poziom : OUT Poziom_Trudnosci; Liczba_Wrogow : OUT Integer);
   PROCEDURE Nowa_Gra (
         Koniec        : IN OUT Boolean;
         Kolory        : IN OUT Character;
         Poziom        : IN     Poziom_Trudnosci;
         Liczba_Wrogow : IN     Integer);
END Gra;

