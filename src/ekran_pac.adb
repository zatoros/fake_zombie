--Pawel Zatorski
--Air II Rok
--gra "Fake Zombi"
--implementacje pakietu "Ekran_Pac"
--opisy funkcji znajduja sie w specyfikacji

with ada.Text_IO; use ada.Text_IO;

PACKAGE BODY Ekran_Pac IS

   FUNCTION Znak(X : IN Character) RETURN Char_Typ IS
      I : Integer;
   BEGIN
      I := Character'pos(X);
      CASE I is
         WHEN 36 =>
               RETURN RAMKA;
         WHEN 45 | 47 | 92 | 124 =>
            return STRZAL;
         WHEN 2 =>
               RETURN GRACZ;
         WHEN 1 =>
            RETURN WROG;
         WHEN 32 =>
               RETURN PUSTE;
         WHEN OTHERS =>
               RETURN INST;
      END CASE;
   END Znak;

   PROCEDURE Dopisz( Data : IN OUT Konsola; X : X_Pos; Y : Y_Pos; ST : String) IS
      Temp : X_Pos := X;
      BEGIN
         FOR I IN St'First .. St'Last LOOP
            Data(Temp,Y) := St(I);
            Temp := Temp+1;
         END LOOP;
      END Dopisz;

   PROCEDURE Odczytaj_Rekordy(Wyniki : OUT Rekordzisci) IS
      Plik : File_Type;
      Temp : String(1..80);
      Converted : Integer;
      X : Integer;
      PROCEDURE Tworz_Domyslny IS
         Plik : File_Type;
      BEGIN
         Create(Plik,Out_File, Plik_Rekordy);
         FOR I IN 1..10 LOOP
            Put_Line(Plik,"ADA");
            Put_Line(Plik,Integer'Image((11-I)*5));
         END LOOP;
         Close(Plik);
  END Tworz_Domyslny;

   BEGIN
      BEGIN
         Open(Plik,In_File, Plik_Rekordy);
      EXCEPTION
         WHEN Name_Error =>
            Tworz_Domyslny;
             Open(Plik,In_File, Plik_Rekordy);
      END;
      FOR I IN 1..10 LOOP
         Get_Line(Plik,Temp,X);
         Wyniki(I).Imie := Temp(1..3);
         Get_Line(Plik,Temp,X);
         Converted := 0;
         FOR  I IN 2..X LOOP
            Converted := 10 * Converted + Character'Pos(temp(I))-Character'Pos('0');
         END LOOP;
         Wyniki(I).Wynik := Converted;
      END LOOP;
      Close(Plik);
   END Odczytaj_Rekordy;

   PROCEDURE Zapisz_Rekordy(Wyniki : IN Rekordzisci) IS
      Plik : File_Type;
   BEGIN
      Open(Plik,Out_File, Plik_Rekordy);
      FOR I IN 1..10 LOOP
            Put_Line(Plik,Wyniki(I).Imie);
            Put_Line(Plik,Integer'Image(Wyniki(I).Wynik));
         END LOOP;
      Close(Plik);
   END Zapisz_Rekordy;

   PROTECTED BODY Ekran IS
      ENTRY Przestaw_Pole(X,X1 : X_Pos; Y,Y1 : Y_Pos) WHEN Czy_Zainicjowane IS
         Temp : Character;
      BEGIN
         Temp := Data(X,Y);
         Data(X,Y) := Data(X1,Y1);
         Data(X1,Y1) := Temp;
              Goto_Xy(X,Y); Wyswietl(Data(X,Y));
              Goto_Xy(X1,Y1); Wyswietl(Data(X1,Y1));
      END Przestaw_Pole;

      FUNCTION Wolne_Pole(X : X_Pos; Y: Y_Pos)
            RETURN Boolean IS
      BEGIN
         RETURN (Znak(Data(X,Y)) = PUSTE);
      END Wolne_Pole;

      FUNCTION Wrogie_Pole(X : X_Pos;Y: Y_Pos)
            RETURN Boolean IS
      BEGIN
         RETURN (Znak(Data(X,Y)) = WROG);
      END Wrogie_Pole;

      FUNCTION Gracza_Pole(X : X_Pos;Y: Y_Pos)
            RETURN Boolean IS
      BEGIN
         RETURN (Znak(Data(X,Y)) = GRACZ);
      END Gracza_Pole;

      FUNCTION Pocisk_Pole(X : X_Pos;Y: Y_Pos)
            RETURN Boolean IS
      BEGIN
         RETURN (Znak(Data(X,Y)) = STRZAL);
         end Pocisk_Pole;

      PROCEDURE Odswiez is
         BEGIN
            Pokaz_Ekran;
         END Odswiez;

         ENTRY Ustaw_Kolory(Tla,Ramek,Inst,Strzal,Gracz,Wrog : Color_Type)
               WHEN Czy_Zainicjowane IS
         BEGIN
            Kolor_Tla  := Tla;
            Kolor_Ramek := Ramek;
            Kolor_Inst := Inst;
            Kolor_Strzal := Strzal;
            Kolor_Gracz := Gracz;
            Kolor_Wrog := Wrog;
            Pokaz_Ekran;
         END Ustaw_Kolory;

         ENTRY Dodaj_Pole(X : X_Pos; Y : Y_Pos; C : Character)
               WHEN Czy_Zainicjowane IS
         BEGIN
            Data(X,Y) := C;
            Goto_Xy(X,Y); Wyswietl(Data(X,Y));
         END Dodaj_Pole;

         ENTRY Inicjuj_Menu
               WHEN NOT Czy_Zainicjowane IS
         BEGIN
            Tworz_Interfejs_Standard;
            Dopisz(Data,21,24,"Strzalki=ruch po menu  Enter=Wybierz   ");
            Dopisz(Data,27,3,"!!!!!!!FAKE ZOMBI!!!!!!!");
            Dopisz(Data,27,4,"by  ZATOR'S ROLL COMPANY");
            Dopisz(Data,27,7,"$$$$$$$$$$$$$$$$$$$$$$$$");
            Dopisz(Data,27,8,"$    *) Nowa Gra       $");
            Dopisz(Data,27,9,"$    =) Tutorial       $");
            Dopisz(Data,27,10,"$    =) Rekordy        $");
            Dopisz(Data,27,11,"$    =) Pomoc          $");
            Dopisz(Data,27,12,"$    =) Koniec         $");
            Dopisz(Data,27,13,"$$$$$$$$$$$$$$$$$$$$$$$$");
            Czy_Zainicjowane := True;
         END Inicjuj_Menu;

         ENTRY Inicjuj_Pomoc
               WHEN NOT Czy_Zainicjowane IS
         BEGIN
            Tworz_Interfejs_Standard;
            FOR I IN 11..68 LOOP
               Data(I,4) := '$';
               Data(I,20):= '$';
            END LOOP;
            FOR I IN 4..20 LOOP
               Data(11,I) := '$';
               Data(68,I) := '$';
            END LOOP;
            Dopisz(Data,31,0,"*****POMOC*****");
            Dopisz(Data,20,24,"       Nacisnij Enter aby wrocic");
            Dopisz(Data,28,6,Character'val(2) & "     POSTAC GRACZA");
            Dopisz(Data,28,8,Character'val(1) & "     ZOMBI");
            Dopisz(Data,26,10,Character'Val(45) & Character'Val(47) & Character'Val(92) & Character'Val(124) & "    POCISKI");
            Dopisz(Data,13,12,"Zabij zombiakow zanim oni dopadna ciebie");
            Dopisz(Data,13,13,"Zniszcz wszystkich by ukonczyc poziom");
            Dopisz(Data,13,14,"W miare uplywu czasu liczba i szybkosc wrogow rosnie");
            Dopisz(Data,13,15,"Poruszaj sie za pomoca strzalek");
            Dopisz(Data,13,16,"By oddac strzal kilknij w odpowiednim kierunku");
            Dopisz(Data,13,17,"POWODZENIA");
            Czy_Zainicjowane := True;
         END Inicjuj_Pomoc;

         ENTRY Inicjuj_Rekordy(Dane_Graczy : IN Rekordzisci)
            WHEN NOT Czy_Zainicjowane IS
         BEGIN
               Tworz_Interfejs_Standard;
               FOR I IN 29..50 LOOP
               Data(I,5) := '$';
               Data(I,19):= '$';
            END LOOP;
            FOR I IN 2..22 LOOP
               Data(29,I) := '$';
               Data(50,I) := '$';
            END LOOP;
            FOR I IN 1..9 LOOP
               Dopisz(Data,31,I+6, I'Img & ") " & Dane_Graczy(I).Imie & " " & Dane_Graczy(I).Wynik'Img);
            END LOOP;
            Dopisz(Data,31,0,"*****REKORDY*****");
            Dopisz(Data,31,16, "10" & ") " & Dane_Graczy(10).Imie & " " & Dane_Graczy(10).Wynik'Img);
            Dopisz(Data,20,24,"       Nacisnij Enter aby wrocic");
            Czy_Zainicjowane := True;
         END Inicjuj_Rekordy;

         ENTRY Inicjuj_Tutorial
               WHEN NOT Czy_Zainicjowane IS
         BEGIN
            Tworz_Interfejs_Standard;
            FOR I IN 1..78 LOOP
               Data(I,21) := '$';
            END LOOP;
            Dopisz(Data,20,24,"       Nacisnij Enter aby wrocic");
            Dopisz(Data,31,0,"*****TUTORIAL*****");
            Czy_Zainicjowane := True;
         END Inicjuj_Tutorial;

         ENTRY Inicjuj_Gre
               WHEN NOT Czy_Zainicjowane IS
         BEGIN
            Tworz_Interfejs_Standard;
            Dopisz(Data,20,24,"       Nacisnij Enter aby skonczyc");
            Dopisz(Data,0,0,"PUNKTY 0");
            Dopisz(Data,13,0,"CZAS 0");
            Czy_Zainicjowane := True;
         END Inicjuj_Gre;

         ENTRY Inicjuj_Temp
               WHEN NOT Czy_Zainicjowane IS
         BEGIN
            Tworz_Interfejs_Standard;
            Dopisz(Data,27,7,"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Dopisz(Data,27,8,"$                              $");
            Dopisz(Data,27,9,"$ *) Liczba Wrogow < 24 >      $");
            Dopisz(Data,27,10,"$ -) Poziom Trudnosci <TRUDNY> $");
            Dopisz(Data,27,11,"$                              $");
            Dopisz(Data,27,12,"$      ENTER - START GRY       $");
            Dopisz(Data,27,13,"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Czy_Zainicjowane := True;
         END Inicjuj_Temp;

         FUNCTION Kolor(G : IN Character) RETURN Color_Type IS
            H : Char_Typ;
         BEGIN
            H := Znak(G);
            CASE H is
               WHEN Ramka =>
                     RETURN Kolor_Ramek;
               WHEN STRZAL =>
                     RETURN Kolor_Strzal;
               WHEN GRACZ =>
                     RETURN Kolor_Gracz;
               WHEN Wrog =>
                     RETURN Kolor_Wrog;
               WHEN Inst =>
                  RETURN Kolor_Inst;
               WHEN Puste =>
                  return Kolor_tla;
            END CASE;
         END Kolor;

         PROCEDURE Pokaz_Ekran IS
         BEGIN
            Clear_Screen(Kolor_Tla);
            FOR I IN Y_Pos'RANGE LOOP
               FOR J IN X_Pos'RANGE LOOP
                  Wyswietl(Data(J,I));
               END LOOP;
            END LOOP;
            Goto_Xy(0,0);
            Set_Cursor(False);
         END Pokaz_Ekran;

         PROCEDURE Wyswietl(X : IN Character) IS
         BEGIN
            Set_Foreground(Kolor(X));
            Put(X);
         END Wyswietl;

         PROCEDURE Tworz_Interfejs_Standard IS
         BEGIN
            FOR I IN X_Pos'RANGE LOOP
               Data(I,Y_pos'first+1) := '$';
               Data(I,(Y_Pos'Last-1)):= '$';
            END LOOP;
            FOR I IN Y_pos'first+1 .. Y_Pos'Last-1 LOOP
               Data(X_Pos'First,I) := '$';
               Data(X_Pos'Last,I) := '$';
            END LOOP;
            Dopisz(Data,0,24,"0,1,2,3=Zmien kolor");
            Dopisz(Data,60,24,"Esc=Szybkie wyjscie");
         END Tworz_Interfejs_Standard;

         FUNCTION Obsluz_Wejscie_Standard(X : Character) RETURN Boolean IS
         BEGIN
            IF  X = '1' THEN
               Ustaw_Kolory(Yellow,Black,Red,Green,Black,Blue);
            ELSIF X = '2' THEN
               Ustaw_Kolory(Red,Black,Yellow,Blue,Green,White);
            ELSIF X = '3' THEN
               Ustaw_Kolory(Light_Blue,Light_Cyan,Gray,Red,Light_Green,Black);
            ELSIF X = '0' THEN
               Ustaw_Kolory(Black,White,White,White,White,White);
            END IF;

            IF Character'Pos(X) = 27 THEN
               RETURN False;
            END IF;
            RETURN True;
         END Obsluz_Wejscie_Standard;

      END Ekran;
   END Ekran_Pac;

