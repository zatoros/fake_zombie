-- Pawel Zatorski
-- Air II rok
-- Gra "Fake Zombi"
--pakiet zawierajacy glownie obiekt chroniony do obslugi ekranu
--oraz inne funkcje wspomoagjace


with nt_console; use nt_console;

PACKAGE Ekran_Pac IS


   TYPE Char_Typ IS (RAMKA,INST,STRZAL,GRACZ,WROG,PUSTE);                              --typ wyliczeniowy opisuje rzeczy jakie sa wyswietlane na ekrania

   TYPE Konsola  IS ARRAY (X_Pos'RANGE,Y_Pos'RANGE) OF Character;                     --Typ Ekranu (Konsola)
   Plik_Rekordy : String := "Hi_Score.txt";
   FUNCTION Znak(X : IN Character) RETURN Char_Typ;                                    --funkcja zwraca zmienna Char_typ na podstawie znaku
   PROCEDURE Dopisz( Data : IN OUT Konsola; X : X_Pos; Y : Y_Pos; ST : String);         --procedura dopsuje do tabliczy ciag znakow we wskazane miejsce

   TYPE Rekordzista IS RECORD                                    --typ rekordowy przechowuje dane do listy wynikow
      Imie : String(1..3);
      Wynik : Integer;
   END RECORD;

   TYPE Rekordzisci IS ARRAY (1..10) OF Rekordzista;       --tablica rekordow sluzy do przechowywania 10  najlepszych wynikow z gry

   PROCEDURE Odczytaj_Rekordy(Wyniki : OUT Rekordzisci);   --procedura czyta z pliku rekordy( badz tworzy domyslny plik) i zapisuje je do tablicy Wyniki
   PROCEDURE Zapisz_Rekordy(Wyniki : IN Rekordzisci);

--Obiekt prezentujace dane z ekranu
   PROTECTED TYPE Ekran IS
      ENTRY Przestaw_Pole(X,X1 : X_Pos; Y,Y1 : Y_Pos);                        -- wejscie zamienia 2 pola i dynamicznie wyswietla zmienione dane
      ENTRY Ustaw_Kolory(Tla,Ramek,Inst,Strzal,Gracz,Wrog : Color_Type);      -- ustawia kolory poszczegolnych czesci interfejsu
      ENTRY Dodaj_Pole(X : X_Pos; Y : Y_Pos; C : Character);                 --wejscie dodaje pole na ekran i dynamicznie wyswietla zmiany
      FUNCTION Wolne_Pole(X : X_Pos; Y: Y_Pos) RETURN Boolean;               -- sprawdza czy pole jest puste(spacja)
      FUNCTION Wrogie_Pole(X : X_Pos;Y: Y_Pos) RETURN Boolean;               --sprawdza czy pole zajete jest przez wroga
      FUNCTION Gracza_Pole(X : X_Pos;Y: Y_Pos) RETURN Boolean;               --sprawdza czy pole zajete jest przez gracza
      FUNCTION Pocisk_Pole(X : X_Pos;Y: Y_Pos) RETURN Boolean;            --sprawdza czy pole zajete jest przez pocisk
      PROCEDURE Odswiez;                                                      --odswieza ekran
         --wejscia inicjujace wyglad poszczegolnych czesci programu
      ENTRY Inicjuj_Gre;
      ENTRY Inicjuj_Tutorial;
      ENTRY Inicjuj_Menu;
      ENTRY Inicjuj_Pomoc;
      ENTRY Inicjuj_Temp;
      ENTRY Inicjuj_Rekordy(Dane_Graczy : IN Rekordzisci);
      FUNCTION Obsluz_Wejscie_Standard(X : Character) RETURN Boolean;         --funckja obsluguje opcje dostepne wspolnie z kazdej czesci programu, zwraca false w wypadku awaryjnego zakonczenia (Esc)
      PRIVATE
      PROCEDURE Pokaz_Ekran;                                                --procedura prywatna pokazuje swoje dane na ekran
      FUNCTION Kolor(G : IN Character) RETURN Color_Type;                     --procedura pomocnicza okresja jaki typ koloru (tla,ramek..) jest przypisany do danego sy,bolu
      PROCEDURE Tworz_Interfejs_Standard;                                    --tworzy elementy interfejsu wspolne dla wszystkich czesci programu
      PROCEDURE Wyswietl(X : IN Character);                                 --procedura pomocnicza dynamicznie wyswietla dana o sprawdzonym kolorze (funkcja Kolor)
      Data : Konsola := (OTHERS => ( OTHERS => ' '));                        --tablica z danymi
      Czy_Zainicjowane : Boolean:= False;                                    --sprawdza czy interfejs ekranu zostal ustawiony
      Kolor_Tla : Color_Type := Black;                                     --kolor tla
      Kolor_Ramek : Color_Type := White;                                   --kolor ramek
      Kolor_Inst : Color_Type := White;                                    --kolor normalnych liter
      Kolor_Strzal : Color_Type := White;                                  --kolor strzalow
      Kolor_Gracz : Color_Type := White;                                     --kolor gracza
      Kolor_Wrog : Color_Type := White;                                      --kolor wroga
   END Ekran;
END Ekran_Pac;








