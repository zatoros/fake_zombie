--Pawel Zatorski
--Air II rok
--gra "Fake Zombi"
--pakiet obslugujacy menu wlasciwa gre

WITH Ada.Calendar; USE Ada.Calendar;
WITH Nt_Console; USE Nt_Console;
WITH Nt_Mouse; USE Nt_Mouse;
WITH Ekran_Pac; USE Ekran_Pac;
WITH Ada.Text_IO; USE Ada.Text_IO;

PACKAGE BODY Gra IS
   --procedura wczytujaca parametry symulacji ;/
   PROCEDURE Wczytaj_Dane_Gra(Kolory : IN Character; Poziom : OUT Poziom_Trudnosci; Liczba_Wrogow : OUT Integer) IS
      Ekran_Temp : Ekran;
      Temp : Boolean;
      Asci : Character;
      Virtualsc : Integer;
      Controlk : Unsigned;
      TYPE Poz IS mod 2;
      Pozycja : Poz := 0;
      BEGIN
         Ekran_Temp.Inicjuj_Temp;
      Temp := Ekran_Temp.Obsluz_Wejscie_Standard(Kolory);
      Poziom := TRUDNY;
         Liczba_Wrogow := 24;
         FOR I IN 60..(X_Pos'Last-1) LOOP
            Ekran_Temp.Dodaj_Pole(I,Y_Pos'Last,' ');
         END LOOP;
      LOOP
            Get_AnyKey(Asci,Virtualsc,Controlk);
            Temp := Ekran_Temp.Obsluz_Wejscie_Standard(Asci);
         EXIT WHEN Key_Name_Text(Virtualsc,Controlk) = "Enter";
         IF Key_Name_Text(Virtualsc,Controlk) = "Down" THEN
               Pozycja := Pozycja + 1;
               Ekran_Temp.Przestaw_Pole(29,29,9,10);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Up" THEN
               Pozycja := Pozycja - 1;
               Ekran_Temp.Przestaw_Pole(29,29,9,10);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Left" THEN
            IF Pozycja = 0 THEN
               IF Liczba_Wrogow > 12 THEN
                     Liczba_Wrogow := Liczba_Wrogow-4;
                     FOR I IN Integer'Image(Liczba_Wrogow)'RANGE LOOP
                        Ekran_Temp.Dodaj_Pole(46+I,9,Integer'Image(Liczba_Wrogow)(I));
                     END LOOP;
               END IF;
            ELSE
               IF Poziom /= Poziom_Trudnosci'Last THEN
                     Poziom := Poziom_Trudnosci'Succ(Poziom);
                     FOR I IN Poziom_Trudnosci'Image(Poziom)'RANGE LOOP
                        Ekran_Temp.Dodaj_Pole(49+I,10,Poziom_Trudnosci'Image(Poziom)(I));
                     END LOOP;
               END IF;
            END IF;
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Right" THEN
            IF Pozycja = 0 THEN
               IF Liczba_Wrogow < 24 THEN
                     Liczba_Wrogow := Liczba_Wrogow+4;
                     FOR I IN Integer'Image(Liczba_Wrogow)'RANGE LOOP
                        Ekran_Temp.Dodaj_Pole(46+I,9,Integer'Image(Liczba_Wrogow)(I));
                     END LOOP;
                  END IF;
            ELSE
               IF Poziom /= Poziom_Trudnosci'First THEN
                     Poziom := Poziom_Trudnosci'Pred(Poziom);
                     FOR I IN Poziom_Trudnosci'Image(Poziom)'RANGE LOOP
                        Ekran_Temp.Dodaj_Pole(49+I,10,Poziom_Trudnosci'Image(Poziom)(I));
                     END LOOP;
               END IF;
            END IF;
         END IF;
      END LOOP;
      END wczytaj_dane_gra;

   --procedura odpowiedzialna za wlasciwa czesc gry
   PROCEDURE Nowa_Gra (Koniec : IN OUT Boolean; Kolory : IN OUT Character; Poziom : IN Poziom_Trudnosci; Liczba_Wrogow: IN Integer) IS

      TYPE Kierunek IS                  --typ opisujacy kierunek ruchy
            (G, GL, GP,L,P,D,DL,DP);
      --zmienna potrzebne do sprawdzenia czy zostal pobity rekord
      Wyniki    :          Rekordzisci;
      Temp      :          Rekordzista;
      Imie_Gracza : String (1 .. 3);
      --Ekran Gry
      Ekran_Gra :          Ekran;
      --stale uzyte w programie
      --rozmiar planszy:
      Xp        : CONSTANT X_Pos       := 1;
      Xk        : CONSTANT X_Pos       := 78;
      Yp        : CONSTANT Y_Pos       := 2;
      Yk        : CONSTANT Y_Pos       := 22;
      --stale napisy wyswietlane przez rozpoczeciem i na zakonczenie
      Intro        : CONSTANT String  := "Nacisnij dowolny Klawisz by rozpoczac";
      Outro_1      : CONSTANT String  := "            KONIEC GRY ";
      Outro_2      : CONSTANT String  := "    Nacisnij Enter by kontynowac";
      Outro_3      : CONSTANT String  := "            Podaj swoje imie          ";

      --zmienne pomocnicze uzyte do obslugi myszli i klawiatury
      I           : Integer;
      X, Y           : Integer;
      Buttons, Controlk    : Unsigned;
      Asci        : Character;
      Virtualsc   : Integer;
      Abnormal    : Boolean         := False;


      --stale i zmienna zwiazane z maxymalna iloscia obiektow na ekranie oraz stanem gry
      Max_Zombi    : CONSTANT Integer := Liczba_Wrogow;
      Max_Pociskow : CONSTANT Integer := 5;
      Punkty       :          Integer := 0;
      PRAGMA Atomic(Punkty);
      Ilosc_Pociskow : Integer := 0;
      Ilosc_Wrogow : Integer := Max_Zombi;
      PRAGMA Atomic(Ilosc_Pociskow);
      PRAGMA Atomic(Ilosc_Wrogow);
      Koniec_Gry : Boolean := False;
      PRAGMA Atomic(Koniec_Gry);
      TYPE Indexp IS mod Max_Pociskow;
      --TYPE Indexz IS mod Max_Zombi;

      --obiekt chroniony sledzacy pozycje gracza, by wrog mogl go namierzyc
      PROTECTED Obsluga_Zderzen IS
         PROCEDURE Ustaw_Gracza (
               X : X_Pos;
               Y : Y_Pos);
         ENTRY Pozycja_Gracza (
               X :    OUT X_Pos;
               Y :    OUT Y_Pos);
      PRIVATE
         Xg : X_Pos;
         Yg : Y_Pos;
         Gracz_Ustawiony : Boolean:= False;
      END Obsluga_Zderzen;

      -- zadanie opisujace postac gracza
      TASK Gracz   IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos);
         ENTRY Ruch (
               K : Kierunek);
         ENTRY Strzal (
               X : X_Pos;
               Y : Y_Pos);
         ENTRY Koniec;
      END Gracz;

      TASK TYPE Pocisk IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos;
               K  : Kierunek);
         ENTRY Koniec;
      END Pocisk;

      TASK Zegarek IS
         ENTRY Start;
         ENTRY Stop;
      END Zegarek;

      TASK body Zegarek IS
         Czas : Integer := 0;
         Takt : CONSTANT Duration := 1.0;
         Nast : Time;
         Czy_Koniec : Boolean := False;
         Opis_Czasu : CONSTANT String := "Czas gry : ";
         Opis_Poziomu : CONSTANT String := "Poziom :";
         Opis_Wrogow : CONSTANT String :="Liczba Wrogow : ";
         Opis_Zabitych : CONSTANT String :="Liczba Zabitych : ";
      BEGIN
         ACCEPT Start DO
            Nast := Clock;
         END Start;
         LOOP
            DELAY UNTIL Nast;
            EXIT WHEN Czy_Koniec;
            SELECT
               ACCEPT Stop DO
                  Czy_Koniec := True;
               END Stop;
            ELSE
               Czas := Czas + 1;
               FOR I IN Integer'Image(Czas)'RANGE LOOP
                  Ekran_Gra.Dodaj_Pole(I+16,0,Integer'Image(Czas)(I));
               END LOOP;
            END SELECT;
            Nast := Nast + Takt;
         END LOOP;

         Ilosc_wrogow := Ilosc_wrogow + 1;
         FOR I IN Poziom_Trudnosci'Image(Poziom)'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(I+41,21,Poziom_Trudnosci'Image(Poziom)(I));
         END LOOP;
         FOR I IN Opis_Poziomu'range LOOP
            Ekran_Gra.Dodaj_Pole(I+22,21,Opis_Poziomu(I));
         END LOOP;

         FOR I IN Integer'Image(Liczba_Wrogow)'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(I+40,20,Integer'Image(Liczba_Wrogow)(I));
         END LOOP;
         FOR I IN Opis_Wrogow'range LOOP
            Ekran_Gra.Dodaj_Pole(I+22,20,Opis_Wrogow(I));
         END LOOP;

         FOR I IN Integer'Image(Punkty)'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(I+40,19,Integer'Image(Punkty)(I));
         END LOOP;
         FOR I IN Opis_Zabitych'range LOOP
            Ekran_Gra.Dodaj_Pole(I+22,19,Opis_Zabitych(I));
         END LOOP;

         FOR I IN Integer'Image(Czas)'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(I+40,18,Integer'Image(Czas)(I));
         END LOOP;
         FOR I IN Opis_Czasu'range LOOP
            Ekran_Gra.Dodaj_Pole(I+22,18,Opis_Czasu(I));
         END LOOP;

         Ilosc_Wrogow := Ilosc_Wrogow -1;

      END Zegarek;


      --typ zadaniowy opisujacy pociski na planszy
      TASK BODY Pocisk IS
         Xpo,
         Xt         :          X_Pos;
         Ypo,
         Yt         :          Y_Pos;
         Cel        :          Kierunek;
         Lot        :          Integer;
         Czy_Koniec :          Boolean  := False;
         Zasieg     : CONSTANT Integer  := 15;
         Szybkosc   : CONSTANT Duration := 0.05;
         Nast       :          Time;
      BEGIN
         LOOP
            EXIT WHEN Czy_Koniec;
            SELECT
               ACCEPT Inicjalizuj (
                     Xs : X_Pos;
                     Ys : Y_Pos;
                     K  : Kierunek) DO
                  Xpo := Xs;
                  Ypo := Ys;
                  Cel := K;
                  Lot := 0;
                  Ilosc_Pociskow := Ilosc_Pociskow+1;
                  CASE K IS
                     WHEN G | D =>
                        Ekran_Gra.Dodaj_Pole(Xs,Ys,Character'Val(124));
                     WHEN L | P =>
                        Ekran_Gra.Dodaj_Pole(Xs,Ys,Character'Val(45));
                     WHEN GL | DP =>
                        Ekran_Gra.Dodaj_Pole(Xs,Ys,Character'Val(92));
                     WHEN GP | DL =>
                        Ekran_Gra.Dodaj_Pole(Xs,Ys,Character'Val(47));
                  END CASE;
               END Inicjalizuj;
            OR
               ACCEPT Koniec DO
                  Czy_Koniec := True;
               END Koniec;
            END SELECT;
            Nast := Clock + Szybkosc;
            EXIT WHEN Czy_Koniec;
            LOOP
               IF Lot = Zasieg OR NOT Ekran_Gra.Pocisk_Pole(Xpo,Ypo) THEN
                  Ekran_Gra.Dodaj_Pole(Xpo,Ypo,' ');
                  EXIT;
               END IF;
               DELAY UNTIL Nast;
               CASE Cel IS
                  WHEN G =>
                     Xt := Xpo;
                     Yt := Ypo-1;
                  WHEN GL=>
                     Xt := Xpo-1;
                     Yt := Ypo-1;
                  WHEN GP =>
                     Xt := Xpo+1;
                     Yt := Ypo-1;
                  WHEN L =>
                     Xt := Xpo-1;
                     Yt := Ypo;
                  WHEN P =>
                     Xt := Xpo+1;
                     Yt := Ypo;
                  WHEN D =>
                     Xt := Xpo;
                     Yt := Ypo+1;
                  WHEN DL =>
                     Xt := Xpo-1;
                     Yt := Ypo+1;
                  WHEN DP =>
                     Xt := Xpo+1;
                     Yt := Ypo+1;
               END CASE;
               IF Xt NOT IN Xp..Xk THEN
                  Xt := Xpo -(Xt-Xpo);
                  CASE Cel IS
                     WHEN GL=>
                        Cel := GP;
                     WHEN GP =>
                        Cel := GL;
                     WHEN L =>
                        Cel := P;
                     WHEN P =>
                        Cel := L;
                     WHEN DL =>
                        Cel := Dp;
                     WHEN DP =>
                        Cel := DL;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
               END IF;
               IF Yt NOT IN Yp..Yk THEN
                  Yt := Ypo - (Yt-Ypo);
                  CASE Cel IS
                     WHEN G =>
                        Cel := D;
                     WHEN GL=>
                        Cel := DL;
                     WHEN GP =>
                        Cel := DP;
                     WHEN D =>
                        Cel := G;
                     WHEN DL =>
                        Cel := GL;
                     WHEN DP =>
                        Cel := GP;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
               END IF;
               CASE Cel IS
                  WHEN G | D =>
                     Ekran_Gra.Dodaj_Pole(Xpo,Ypo,Character'Val(124));
                  WHEN L | P =>
                     Ekran_Gra.Dodaj_Pole(Xpo,Ypo,Character'Val(45));
                  WHEN GL | DP =>
                     Ekran_Gra.Dodaj_Pole(Xpo,Ypo,Character'Val(92));
                  WHEN GP | DL =>
                     Ekran_Gra.Dodaj_Pole(Xpo,Ypo,Character'Val(47));
               END CASE;
               IF Ekran_Gra.Wolne_Pole(Xt,Yt) THEN
                  Ekran_Gra.Przestaw_Pole(Xpo,Xt,Ypo,Yt);
                  Xpo := Xt;
                  Ypo := Yt;
               ELSIF Ekran_Gra.Wrogie_Pole(Xt,Yt) THEN
                  Ekran_Gra.Dodaj_Pole(Xt,Yt,' ');
                  Lot := Zasieg-1;
               END IF;
               Lot := Lot+1;
               Nast := Nast + Szybkosc;
            END LOOP;
            Ilosc_Pociskow := Ilosc_Pociskow-1;
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznany wyjatek w ZADANIU POCISK");
      END Pocisk;

      Pociski : ARRAY (Indexp) OF Pocisk;

       --typ zadaniowy opisujacy wrogow na planszy
      TASK TYPE Zombi IS
         ENTRY Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos);
      END Zombi;

      TASK BODY Zombi IS
         Xp,
         Xz,
         Xt,
         Xg       : X_Pos;
         Yp,
         YZ,
         Yt,
         Yg       : Y_Pos;
         Vx,
         Vy       : Integer;
         Szybkosc : Duration := 1.0  * Duration(Poziom_Trudnosci'Pos(Poziom) +1); --POZIOM
         Nastepny : Duration := 2.0;
         Nast     : Time;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos) DO
            Xp := Xs;
            Yp := Ys;
         END Inicjalizuj;
         LOOP

            Xz := Xp;
            Yz := Yp;
            Ekran_Gra.Dodaj_Pole(Xz,Yz,Character'Val(1));
            Nast := Clock;
            LOOP
               DELAY UNTIL Nast;
               IF NOT Ekran_Gra.Wrogie_Pole(Xz,YZ) THEN
                  Punkty := Punkty +1;
                  FOR I IN Integer'Image(Punkty)'RANGE LOOP
                     Ekran_Gra.Dodaj_Pole(6+I,0,Integer'Image(Punkty)(I));
                  END LOOP;
                  EXIT;
               END IF;
               Obsluga_Zderzen.Pozycja_Gracza(Xg,Yg);
               Vx := Xg-Xz;
               Vy := Yg-Yz;
               IF Vx > 0 THEN
                  Xt := Xz+1;
               ELSIF Vx < 0 THEN
                  Xt := Xz-1;
               ELSE
                  Xt := Xz;
               END IF;
               IF Vy > 0 THEN
                  Yt := Yz+1;
               ELSIF Vy < 0 THEN
                  Yt := Yz-1;
               ELSE
                  Yt := Yz;
               END IF;
               IF Ekran_Gra.Wolne_Pole(Xt,Yt) THEN
                  Ekran_Gra.Przestaw_Pole(Xz,Xt,Yz,Yt);
                  Xz := Xt;
                  Yz := Yt;
               ELSIF Ekran_Gra.Pocisk_Pole(Xt,Yt) THEN
                  Ekran_Gra.Dodaj_Pole(Xz,Yz,' ');
                  Ekran_Gra.Dodaj_Pole(Xp,Yp,' ');
                  Punkty := Punkty +1;
                  FOR I IN Integer'Image(Punkty)'RANGE LOOP
                     Ekran_Gra.Dodaj_Pole(6+I,0,Integer'Image(Punkty)(I));
                  END LOOP;
                  EXIT;
               ELSIF Ekran_Gra.Gracza_Pole(Xt,Yt) THEN
                  Ekran_Gra.Dodaj_Pole(Xt,Yt,'*');
                  Koniec_Gry := True;
               END IF;
               EXIT WHEN Koniec_Gry;
               Nast := Nast + Szybkosc;
            END LOOP;
            EXIT WHEN Koniec_Gry;
            Szybkosc := Szybkosc/(2.0  -Duration( Duration(Poziom_Trudnosci'Pos(Poziom)) * 0.25 ) );
            DELAY Nastepny;
            IF Nastepny /= 0.0 THEN
               Nastepny := Nastepny - 0.1;
            END IF;
         END LOOP;
         Ilosc_Wrogow := Ilosc_Wrogow-1;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznany wyjatek w ZADANIU ZOMBI");
      END Zombi;

      TASK BODY Gracz IS
         Xg,
         Xt         :          X_Pos;
         Yg,
         Yt         :          Y_Pos;
         Kpo        :          Kierunek;
         Nast       :          Time;
         Czy_Koniec :          Boolean  := False;
         Przerwa    : CONSTANT Duration := 0.1;
         I          :          Indexp   := 0;
      BEGIN
         ACCEPT Inicjalizuj (
               Xs : X_Pos;
               Ys : Y_Pos) DO
            Xg := Xs;
            Yg := Ys;
            Ekran_Gra.Dodaj_Pole(Xg,Yg,Character'Val(2));
            Obsluga_Zderzen.Ustaw_Gracza(Xg,Yg);
         END Inicjalizuj;
         Nast:= Clock;
         LOOP
            DELAY UNTIL Nast;
            SELECT
               ACCEPT Ruch (
                     K : Kierunek) DO
                  CASE K IS
                     WHEN G=>
                        Xt := Xg;
                        Yt := Yg-1;
                     WHEN L=>
                        Xt := Xg-1;
                        Yt := Yg;
                     WHEN P=>
                        Xt := Xg+1;
                        Yt := Yg;
                     WHEN D=>
                        Xt := Xg;
                        Yt := Yg+1;
                     WHEN OTHERS =>
                        NULL;
                  END CASE;
                  IF Ekran_Gra.Wolne_Pole(Xt,Yt) THEN
                     Ekran_Gra.Przestaw_Pole(Xg,Xt,Yg,Yt);
                     Xg := Xt;
                     Yg := Yt;
                     Obsluga_Zderzen.Ustaw_Gracza(Xg,Yg);
                  END IF;
               END Ruch;
            OR
               ACCEPT Strzal (
                     X : X_Pos;
                     Y : Y_Pos) DO
                  IF Ilosc_Pociskow < Max_Pociskow THEN
                     IF X > Xg AND Y = Yg THEN
                        Xt := Xg+1;
                        Yt:=Yg;
                        Kpo :=P;
                     ELSIF X > Xg AND Y < Yg THEN
                        Xt:=Xg+1;
                        Yt:=Yg-1;
                        Kpo:=GP;
                     ELSIF X > Xg AND Y > Yg THEN
                        Xt:=Xg+1;
                        Yt:=Yg+1;
                        Kpo:=DP;
                     ELSIF X = Xg AND Y < Yg THEN
                        Xt:=Xg;
                        Yt:=Yg-1;
                        Kpo:=G;
                     ELSIF X = Xg AND Y > Yg THEN
                        Xt:=Xg;
                        Yt:=Yg+1;
                        Kpo:=D;
                     ELSIF X < Xg AND Y = Yg THEN
                        Xt:=Xg-1;
                        Yt:=Yg;
                        Kpo:=L;
                     ELSIF X < Xg AND Y < Yg THEN
                        Xt:=Xg-1;
                        Yt:=Yg-1;
                        Kpo:=GL;
                     ELSIF X < Xg AND Y > Yg THEN
                        Xt:=Xg-1;
                        Yt:=Yg+1;
                        Kpo:=DL;
                     END IF;
                     IF Ekran_Gra.Wolne_Pole(Xt,Yt) THEN
                        Pociski(I).Inicjalizuj(Xt,Yt,Kpo);
                        I := I+1;
                     END IF;
                  END IF;
               END Strzal;
            OR
               ACCEPT Koniec DO
                  Czy_Koniec := True;
               END Koniec;
            END SELECT;
            EXIT WHEN Czy_Koniec;
            Nast := Nast + Przerwa;
         END LOOP;
      EXCEPTION
         WHEN OTHERS =>
            Put("Nieznany wyjatek w ZADANIU GRACZ");
      END Gracz;

      Wrogowie : ARRAY (0..Max_Zombi-1) OF Zombi;



      PROTECTED BODY Obsluga_Zderzen IS
         PROCEDURE Ustaw_Gracza (
               X : X_Pos;
               Y : Y_Pos) IS
         BEGIN
            Xg := X;
            Yg := Y;
            Gracz_Ustawiony := True;
         END Ustaw_Gracza;

         ENTRY Pozycja_Gracza (
               X :    OUT X_Pos;
               Y :    OUT Y_Pos) WHEN Gracz_Ustawiony IS
         BEGIN
            X := Xg;
            Y := Yg;
         END Pozycja_Gracza;
      END Obsluga_Zderzen;

   BEGIN
      Ekran_Gra.Inicjuj_Gre;
      Koniec := NOT Ekran_Gra.Obsluz_Wejscie_Standard(Kolory);
      --tu nastepuje inicjalizacja stalych pozycji startowych wrogow
      FOR I IN 0..((Max_Zombi/4)-1) LOOP
         Wrogowie(I).Inicjalizuj(I+14,3);
         Wrogowie(I+Max_Zombi/4).Inicjalizuj(I+14,20);
         Wrogowie(I+Max_Zombi/2).Inicjalizuj(I+60,3);
         Wrogowie(I+Max_Zombi*3/4).Inicjalizuj(I+60,20);
      END LOOP;
      --drukowanie informacji startowych
      FOR I IN Intro'RANGE LOOP
         Ekran_Gra.Dodaj_Pole(20+I,15,Intro(I));
      END LOOP;
      Get_AnyKey(Asci,Virtualsc,Controlk);
      FOR I IN Intro'RANGE LOOP
         Ekran_Gra.Dodaj_Pole(20+I,15,' ');
      END LOOP;
      Gracz.Inicjalizuj(40,12);
      Zegarek.Start;
      LOOP
         Get_Anything(X,Y,Buttons,Asci,Virtualsc,Controlk);
               --awaryjne zakonczenie programu
         IF NOT Ekran_Gra.Obsluz_Wejscie_Standard(Asci) THEN
            ABORT
            Gracz;
            FOR I IN Indexp LOOP
               ABORT Pociski(I);
            END LOOP;
            FOR I IN 0..Max_Zombi-1 LOOP
               ABORT Wrogowie(I);
            END LOOP;
            ABORT Zegarek;
            Abnormal := True;
            Koniec := True;
            EXIT;
         END IF;
         IF Asci IN '0'..'3' THEN
            Kolory := Asci;
         END IF;

         IF Koniec_Gry OR Key_Name_Text(Virtualsc,Controlk) = "Enter" THEN
            Koniec_Gry := True;
            Gracz.Koniec;
            Zegarek.Stop;
            FOR I IN Indexp LOOP
               Pociski(I).Koniec;
            END LOOP;
            EXIT;
         ELSIF Buttons = From_Left_1st_Button_Pressed THEN
            Gracz.Strzal(X,Y);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Left" THEN
            Gracz.Ruch(L);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Right" THEN
            Gracz.Ruch(P);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Up" THEN
            Gracz.Ruch(G);
         ELSIF Key_Name_Text(Virtualsc,Controlk) = "Down" THEN
            Gracz.Ruch(D);
         END IF;
      END LOOP;

      IF NOT Abnormal THEN
         --Tu sprawdzane jest czy zostal pobity rekord i w razie pobicia wpisywanie danych gracza
         Odczytaj_Rekordy(Wyniki);
         FOR I IN Outro_1'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(22+I,15,Outro_1(I));
         END LOOP;
         FOR I IN Outro_2'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(22+I,16,Outro_2(I));
         END LOOP;
         Get_AnyKey(Asci,Virtualsc,Controlk);
         WHILE Key_Name_Text(Virtualsc,Controlk) /= "Enter" LOOP
            Get_AnyKey(Asci,Virtualsc,Controlk);
         END LOOP;
         FOR I IN Outro_1'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(22+I,15,' ');
         END LOOP;
         FOR I IN Outro_2'RANGE LOOP
            Ekran_Gra.Dodaj_Pole(22+I,16,' ');
         END LOOP;

         IF Wyniki(Wyniki'Last).Wynik < Punkty THEN
            FOR I IN Outro_3'RANGE LOOP
               Ekran_Gra.Dodaj_Pole(22+I,16,Outro_3(I));
            END LOOP;
            WHILE Ilosc_Wrogow /= 0 LOOP
               NULL;
            END LOOP;
            Set_Cursor(True);
            Goto_XY(40,17);
            Imie_Gracza := "---";
            FOR I IN Imie_Gracza'RANGE LOOP
               Ekran_Gra.Dodaj_Pole(Where_X,Where_Y,Imie_Gracza(I));
               Goto_XY(I+40,17);
            END LOOP;
            Goto_XY(40,17);
            LOOP
               Get_AnyKey(Asci,Virtualsc,Controlk);
               EXIT WHEN Key_Name_Text(Virtualsc,Controlk) = "Enter";
               IF Key_Name_Text(Virtualsc,Controlk) = "Left" and Where_X > 40 THEN
                  Goto_XY(Where_X - 1,Where_Y);
               ELSIF Key_Name_Text(Virtualsc,Controlk) = "Right" and Where_X < 42 THEN
                  Goto_XY(Where_X + 1,Where_Y);
               ELSIF Asci IN 'A'..'z'and Where_X < 43 THEN
                  Ekran_Gra.Dodaj_Pole(Where_X,Where_Y,Asci);
                  Imie_Gracza(Where_X-40) := Asci;
               END IF;
            END LOOP;
            Set_Cursor(False);
            Wyniki(Wyniki'Last).Wynik := Punkty;
            Wyniki(Wyniki'Last).Imie := Imie_Gracza;
                   --wsortowanie wyniku w liste
            I := Wyniki'Last;
            WHILE I /= Wyniki'First LOOP
               IF Wyniki(I).Wynik > Wyniki(I-1).Wynik THEN
                  Temp := Wyniki(I);
                  Wyniki(I) := Wyniki(I-1);
                  Wyniki(I-1) := Temp;
               END IF;
               I := I-1;
            END LOOP;

            Zapisz_Rekordy(Wyniki);
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS =>
         Put("Nieznany wyjatek w PROCEDURZE GRY");
   END Nowa_Gra;
END Gra;



